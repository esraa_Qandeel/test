$(document).ready(function () {
    initPage();
});

/* FUNCTIONS DEFINITION */
function initPage() {

}


/* CENTER DIVS VERTICALLY */
function center_divs() {
    var temp;
    var win_height = $(window).height();
    var div_height = $('.vertical_center').height();
    var center_div = (win_height / 2) - (div_height / 2);
    $('.vertical_center').css('margin-top', center_div);
    $(window).resize(function () {
        clearTimeout(temp);
        temp = setTimeout(function () {
            win_height = $(window).height();
            div_height = $('.vertical_center').height();
            center_div = (win_height / 2) - (div_height / 2);
            $('.vertical_center').css('margin-top', center_div);
        }, 200);
    });
}

/* VALIDATE NUMBERS */
function validate_numbers(element) {
    element.keydown(function (e) {
        // ALLOW: BACKSPACE, DELETE, TAB, ESCAPE, ENTER AND .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // ALLOW: CTRL+A, COMMAND+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) ||
                // ALLOW: HOME, END, LEFT, RIGHT, DOWN, UP
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // LET IT HAPPEN
            return true;
        }
        // ENSURE THAT IT IS A NUMBER
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105))
            return false;
        else {
            return true;
        }
    });
}

/* VALIDATE LENGTH */
function validate_length(element, length) {
    // SET MAX LENGTH
    element.keydown(function (e) {

        // ALLOW: BACKSPACE, DELETE, TAB, ESCAPE, ENTER AND .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // ALLOW: CTRL+A, COMMAND+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) ||
                // ALLOW: HOME, END, LEFT, RIGHT, DOWN, UP
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // LET IT HAPPEN
            return true;
        }
        if (element.val().length > length - 1) {
            e.preventDefault();
        }
    });
}

/* VALIDATE EMAIL */
function validate_email(value) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(value);
}